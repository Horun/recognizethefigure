﻿using System.Collections.Generic;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.GameFigure
{
    public abstract class AngularFigure : Figure
    {
        private readonly Painter _painter;

        protected AngularFigure()
        {
            _painter = new Painter();
        }

        protected void DrawFigureSides(List<Vector2> vertices, TextureManager textureManager)
        {
            for (var i = 0; i < vertices.Count; i++)
            {
                var nextPointIndex = (i + 1) < vertices.Count ? i + 1 : 0;
                DrawSide(vertices[i], vertices[nextPointIndex], textureManager);
            }
        }

        private void DrawSide(Vector2 point1, Vector2 point2, TextureManager textureManager)
        {
            var sidePoints = GetterLinePoints.GetPixelsPositonOnTextureBetween(point1, point2, textureManager);
            _painter.Draw(sidePoints, textureManager);
        }

        public override void Draw(TextureManager textureManager, Vector2 position)
        {
            var vertices = GetVertices(position);
            DrawFigureSides(vertices, textureManager);
            SetPoints(vertices);
        }

        protected abstract List<Vector2> GetVertices(Vector2 middlePoint);

    }
}
