﻿using Assets.Scripts.FigureEditor;
using Assets.Scripts.GameFigure.ConcretFigure;
using Assets.Scripts.GameManager;

namespace Assets.Scripts.GameFigure
{
    public class FigureGetter
    {   
        private Figure[] _figures;
        private int _currentFigure = -1;

        public FigureGetter(float figureSize) 
        {
            SetFiguresForGame(figureSize);
        }

        public void Reset()
        {
            _currentFigure = -1;
        }

        private void SetFiguresForGame(float figureSize)
        {
            _figures = new Figure[]
            {
                new EquilateralFigure(4, figureSize, 45),
                new EquilateralFigure(3, figureSize, 0),
                new RandomPolygon(figureSize, 3),
                new EquilateralFigure(5, figureSize, 45),
                new Star(15,figureSize),            
                new EquilateralFigure(4, figureSize, 0),
                new RandomPolygon(figureSize, 3),
                new RandomPolygon(figureSize, 5),
                new RandomPolygon(figureSize, 4),
                new Star(30, figureSize),
                new RandomPolygon(figureSize, 3),
                new RandomPolygon(figureSize, 6),
                new RandomPolygon(figureSize, 4),
            };

        }

        public Figure GetFigure(GameMode gameMode)
        {
            return gameMode == GameMode.EditorMode
                ? GetLastFigureFormEditor()
                : _figures[GetNextFigureIndex()];
        }

        private int GetNextFigureIndex()
        {
            _currentFigure = _currentFigure + 1 < _figures.Length ? _currentFigure + 1 : 0;
            return _currentFigure;
        }

        private Figure GetLastFigureFormEditor()
        {
            return new EditorFigure(FigureSaver.LastSavedFigureName);
        }

    }
}

