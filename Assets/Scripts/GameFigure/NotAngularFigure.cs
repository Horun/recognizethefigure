﻿using System.Collections.Generic;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.GameFigure
{
    public abstract class NotAngularFigure : Figure
    {
        public override void Draw(TextureManager textureManager, Vector2 position)
        {
            var figurePoints = GetAllPoints(position);
            SetPoints(figurePoints);
            textureManager.ChangePixels(GetAllPoints(position), Color.black);
            
        }

        protected abstract List<Vector2> GetAllPoints(Vector2 middlePoint);
    }
}
