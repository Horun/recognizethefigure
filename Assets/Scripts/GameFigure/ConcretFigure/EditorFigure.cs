﻿using System.Collections.Generic;
using Assets.Scripts.FigureEditor;
using UnityEngine;

namespace Assets.Scripts.GameFigure.ConcretFigure
{
    public class EditorFigure : AngularFigure
    {
        private readonly string _figureName;
        private readonly FigureSaver _figureSaver;

        public EditorFigure(string figureName)
        {
            _figureName = figureName;
            _figureSaver = new FigureSaver();
        }

        protected override List<Vector2> GetVertices(Vector2 middlePoint)
        {
            return _figureSaver.GetSavedFigureVertices(_figureName);
        }
    }
}
