﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameFigure.ConcretFigure
{
    public class EquilateralFigure : AngularFigure
    {
        private readonly int _anglesCount;
        private readonly float _radius;
        private readonly float _rotationAngle;

        public EquilateralFigure(int anglesCount, float size, float rotatAngle)
        {
            _anglesCount = anglesCount;
            _radius = size/2f;
            _rotationAngle = rotatAngle;
        }

        protected override List<Vector2> GetVertices(Vector2 centralPoint)
        {
            var angle = 360f / _anglesCount; // its angle between radius, no figure angle
            var vertices = new List<Vector2>();

            for (var i = 0; i < _anglesCount; i++)
            {
                var radian = Mathf.PI / 180 * (angle * i + _rotationAngle);
                var posX = _radius * Mathf.Cos(radian) + centralPoint.x;
                var posY = _radius * Mathf.Sin(radian) + centralPoint.y;
                vertices.Add(new Vector2(posX, posY));
            }
            return vertices;
        }
    }
}
