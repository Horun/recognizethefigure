﻿using System;
using System.Collections.Generic;
using Assets.Scripts.GameTexture;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.GameFigure.ConcretFigure
{
    public class RandomPolygon: AngularFigure
    {
        private readonly int _anglesCount;
        private readonly float _figureRadius;
        private readonly float _rotationAngle;

        public RandomPolygon(float size, int sideCount = 0)
        {
             _anglesCount = IsAnglesCountInPossibleRange(sideCount) ? sideCount : Random.Range(3, 5);
            _figureRadius = size/2f;
            _rotationAngle = Random.Range(0, 360);
        }

        private bool IsAnglesCountInPossibleRange(int sideCount)
        {
            return sideCount >= 3 && sideCount < 6; 
        }
      
        protected override List<Vector2> GetVertices(Vector2 middlePoint)
        {
            return GetterRandomVertices.GetRandomFigureVertices(_figureRadius, middlePoint, _anglesCount);
        }
    }
}
