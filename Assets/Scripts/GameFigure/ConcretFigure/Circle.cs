﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameFigure.ConcretFigure
{
    public class Circle : NotAngularFigure
    {
        private readonly float _radius;

        public Circle(float radius)
        {
            _radius = radius;
        }

        protected override List<Vector2> GetAllPoints(Vector2 middlePoint)
        {
            var points = new List<Vector2>();

            for (var i = 0; i < 360; i++)
            {
                var radian = Mathf.PI / 180 * i;
                var posX = _radius * Mathf.Cos(radian) + middlePoint.x;
                var posY = _radius * Mathf.Sin(radian) + middlePoint.y;
                points.Add(new Vector2(posX, posY));
            }
            return points;
        }
    }
}
