﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameFigure.ConcretFigure
{
    public class Star : AngularFigure
    {
        private readonly int _verticesCount;
        private readonly float _rotationAngle;
        private readonly float _radius;

        public Star( float rotationAngle, float size, int vertices = 5) // standart star have 5 vertices, but we can make also jews start
        {
            _verticesCount = vertices;
            _rotationAngle = rotationAngle;
            _radius = size/2f;
        }

        protected override List<Vector2> GetVertices(Vector2 middlePoint)
        {
            var angle = 360f / _verticesCount; // its angle between radius, no figure angle
            var vertices = new List<Vector2>();

            for (var i = 0; i < _verticesCount; i++)
            {
                var radian = Mathf.PI / 180 * (angle * i*2 + _rotationAngle);
                var posX = _radius * Mathf.Cos(radian) + middlePoint.x;
                var posY = _radius * Mathf.Sin(radian) + middlePoint.y;
                vertices.Add(new Vector2(posX, posY));
            }
            return vertices;
        }
    }
}
