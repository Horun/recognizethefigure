﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameFigure.ConcretFigure
{
    public class GetterRandomVertices
    {
        public static List<Vector2> GetRandomFigureVertices(float approximalRadius,Vector2 centralPoint , int sidesCount = 0)
        {
            if (sidesCount < 3)
                sidesCount = Random.Range(3, 6);

            var angle = 360f / sidesCount; // its angle between radius, no figure angle

            var vertices = new List<Vector2>();
            float rotationAngle = Random.Range(0, 45);

            for (var i = 0; i < sidesCount; i++)
            {
                var radian = Mathf.PI / 180 * (angle * i + rotationAngle);
                var posX = Random.Range(0.6f, 1.3f) * approximalRadius * Mathf.Cos(radian) + centralPoint.x;
                var posY = Random.Range(0.6f, 1.3f) * approximalRadius * Mathf.Sin(radian) + centralPoint.y;
                vertices.Add(new Vector2(posX, posY));
            }

            return vertices;
        }
    }
}
