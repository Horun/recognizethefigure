﻿using System.Collections.Generic;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.GameFigure
{
    public abstract class Figure
    {
        public readonly List<Vector2> FigurePoints = new List<Vector2>();

        protected void SetPoints(List<Vector2> points)
        {
            FigurePoints.Clear();
            FigurePoints.AddRange(points);
        }

        public abstract void Draw(TextureManager textureManager, Vector2 position);
    }
}
