﻿using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.GameFigure
{
    public class FigureShower : MonoBehaviour
    {
        public TextureManager TextureForShowing;
        private Vector2 _pointForShowing;

        public void Init()
        {
            _pointForShowing = new Vector2(TextureForShowing.TextureWidth/2f, TextureForShowing.TextureHeight/2f);
        }

        public void ShowFigure(Figure figure)
        {
            TextureForShowing.Clean();
            figure.Draw(TextureForShowing, _pointForShowing);
        }
    }
}
