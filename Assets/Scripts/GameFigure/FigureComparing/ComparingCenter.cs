﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameTexture;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.GameFigure.FigureComparing
{
    public class ComparingCenter
    {
        private readonly float _deviationKoeficient;
        private int _currentPixelDeviation;// pixels count for possibile deviation figure sides
        private readonly DividerIntoVectors _dividerIntoVectors = new DividerIntoVectors();
        private TextureManager _textureManager;

        public ComparingCenter(float deviationKoeficient)
        {
            _deviationKoeficient = deviationKoeficient;
        }

        public bool CheckFiguresOnSimilarity(List<Vector2> figure1, List<Vector2> figure2)
        {
            var sidesCountF1 = _dividerIntoVectors.CheckFigureVertices(figure1).Count;
            var sidesCountF2 = _dividerIntoVectors.CheckFigureVertices(figure2).Count;

            if (!IsMakeSenceCompareFigures(sidesCountF1, sidesCountF2))
                return false;

            MoveFigureToSpecialPosition(ref figure1);
            MoveFigureToSpecialPosition(ref figure2);
           
            MakeFiguresSameInSize(ref figure1, ref figure2);

            ConcatinateVertices(ref figure1, false);
            ConcatinateVertices(ref figure2, true);

            return AreFiguresSimilar(figure1, figure2);
        }

        private bool IsMakeSenceCompareFigures(int sidesCountFig1, int sidesCountFig2)
        {
            var bothAreNotLine = sidesCountFig1 > 2 || sidesCountFig1 > 2;
            var haveNearlySideCounts = Mathf.Abs(sidesCountFig2 - sidesCountFig1) <= 1;

            return (bothAreNotLine && haveNearlySideCounts);
        }

        private int GetFigureHeight(List<Vector2> figurePoints)
        {
            return (int) (figurePoints.Max(point => point.y) - figurePoints.Min(point => point.y));
        }

        private void MoveFigureToSpecialPosition(ref List<Vector2> figurePoints)
        {
            //left lower corner
            var changed = new List<Vector2>();
            var minX = figurePoints.Min(point => point.x);
            var minY = figurePoints.Min(point => point.y);
            var distanceToStartPos = new Vector2(minX, minY);

            for (var i = 0; i < figurePoints.Count; i++)
            {
                changed.Add(figurePoints[i] - distanceToStartPos);
            }
            figurePoints = changed;
        }

        private void MakeFiguresSameInSize(ref List<Vector2> figure1, ref List<Vector2> figure2)
        {
            float f1Height = GetFigureHeight(figure1);
            float f2Height = GetFigureHeight(figure2);

            float scaleRatio = f1Height > f2Height ? f1Height/f2Height : f2Height/f1Height;
            if (f1Height > f2Height)
                DecreaseFigureInSize(ref figure1, scaleRatio);
            else
                DecreaseFigureInSize(ref figure2, scaleRatio);      
        }

        private void DecreaseFigureInSize(ref List<Vector2> figure, float ratio)
        {
            for (var i = 0; i < figure.Count; i++)
            {     
                figure[i] = new Vector2(figure[i].x/ratio , figure[i].y/ratio);
            }
        }

        private void ConcatinateVertices(ref List<Vector2> vertices, bool isPLayerFigure)
        {
            if (_textureManager == null)
                _textureManager = Object.FindObjectsOfType<TextureManager>()[1];

            var figureAllPoints = new List<Vector2>();
            for (int i = 0; i < vertices.Count; i++)
            {
                if(i==vertices.Count-1 && isPLayerFigure)
                    break;

                var nextIndex = i < vertices.Count - 1 ? i + 1 : 0;
                figureAllPoints.AddRange(GetterLinePoints.GetPixelsPositonOnTextureBetween(vertices[i], vertices[nextIndex], _textureManager));
            }

            vertices = figureAllPoints;
        }

        private bool AreFiguresSimilar(List<Vector2> figure1, List<Vector2> figure2)
        {
            var similar = (IsFigureHasAnalogsForAllPointsInOtherFigure(figure1, figure2)) &&
                   (IsFigureHasAnalogsForAllPointsInOtherFigure(figure2, figure1));
          //  Debug.Log("Figures are similar : " + similar);
            return similar;
        }

        private int[,] ConvertFigureToPixel2DArray(List<Vector2> figurePoints)
        {
            var yMax =(int) figurePoints.Max(point => point.y);
            var xMax =(int) figurePoints.Max(point => point.x);
            var figInPixels = new int[yMax+1, xMax+1];
            for (var i = 0; i < figurePoints.Count; i++)
            {
                figInPixels[(int)figurePoints[i].y, (int)figurePoints[i].x] = 1; // 1 - its figure1 point, 0 - free pixel
            }
            return figInPixels;
        }

        private bool IsFigureHasAnalogsForAllPointsInOtherFigure(List<Vector2> figure1, List<Vector2> figure2)
        {
            var fig2As2DArray = ConvertFigureToPixel2DArray(figure2);
            _currentPixelDeviation = (int) ( GetFigureHeight(figure2)*_deviationKoeficient);// in this moment fig1 and fig scale to same size, thats why no diff btw height F1 and height F2
            
            for (var i = 0; i < figure1.Count; i++)
            {
                var point = figure1[i];
                if (!IsPointHaveNearlyPointOfOtherFigure((int)point.x, (int)point.y, fig2As2DArray))
                    return false;
            }

            return true;
        }

        private bool IsPointHaveNearlyPointOfOtherFigure(int pointPosX, int pointPosY, int[,] otherAs2D)
        {
            for (var y = pointPosY- _currentPixelDeviation; (y < pointPosY+ _currentPixelDeviation) && y< otherAs2D.GetLength(0) ; y++)
                for (var x = pointPosX - _currentPixelDeviation; (x < pointPosX + _currentPixelDeviation) && x < otherAs2D.GetLength(1); x++)
                {
                    if(x<0 || y<0)
                        continue;
                    if (otherAs2D[y, x] == 1)
                        return true;
                }

            return false;
        }
    }

    
}