﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.TemporaryHelpers;
using UnityEngine;

namespace Assets.Scripts.GameFigure.FigureComparing
{
    public class DividerIntoVectors  {

        private readonly float _angleRange = 15f; // for correction player curves side
        private readonly float _minSideLength = 20f;

        public List<Vector2> CheckFigureVertices(List<Vector2> figurePoints)
        {
            var vertices = ConcatSimilarVectors(figurePoints);
            if (vertices.Count < 3)
                return vertices;

            for (int i = 0; i < 20; i++)
            {
                var concatinated = ConcatSimilarVectors(vertices);
                if (concatinated.Count >= 3)
                    vertices = concatinated;
                else
                    break;
            }
           
            return vertices;
        }

        private List<Vector2> ConcatSimilarVectors(List<Vector2> figurePoints)
        {
            var vertices = new List<Vector2>();
            var startPoint = figurePoints[0];
            var checkedVectors = new List<Vector2>();
            for (int i = 0; i < figurePoints.Count; i++)
            {
                var nextPoint = i < figurePoints.Count - 1 ? i + 1 : 0;
                var sideCandidate = figurePoints[nextPoint] - startPoint;

                if (IsVectorDeviate(sideCandidate, checkedVectors) && sideCandidate.magnitude > _minSideLength)
                {
                    vertices.Add(startPoint);
                    checkedVectors.Clear();
                    startPoint = figurePoints[i];
                    i--;
                }
                else
                {
                    checkedVectors.Add(sideCandidate);
                }

                if (i == figurePoints.Count - 1 && vertices.Count >= 2)
                {
                    var firstSide = vertices[1] - vertices[0];

                    if (!IsVectorDeviate(firstSide, sideCandidate))
                        vertices.RemoveAt(0);

                    vertices.Add(startPoint);
                }
            }
            return vertices;
        } 

        private bool IsVectorDeviate(Vector2 target, List<Vector2> others)
        {
            return others.Any(t => IsVectorDeviate(target, t));
        }

        private bool IsVectorDeviate(Vector2 target, Vector2 other)
        {
            return Vector2.Angle(target, other) > _angleRange - target.magnitude*0.02;
        }
    }
}

