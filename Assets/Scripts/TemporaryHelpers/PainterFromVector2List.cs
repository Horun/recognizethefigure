﻿using System.Collections.Generic;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.TemporaryHelpers
{
    public class PainterFromVector2List : MonoBehaviour
    {
        public TextureManager TextureManager;

        public void Paint(List<Vector2> figure, Color col)
        {
            for (int i = 0; i < figure.Count; i++)
            {
                TextureManager.ChangePixels(figure, col);
            }
        }
    }
}
