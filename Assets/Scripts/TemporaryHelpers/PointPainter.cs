﻿using System.Collections.Generic;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.TemporaryHelpers
{
    public class PointPainter : MonoBehaviour
    {
        public TextureManager TextureManager;

        public void PrintPoint(Vector2 point, Color col)
        {
            TextureManager.ChangePixels(GetSquareAroundPoint(point), col);        
        }

        private List<Vector2> GetSquareAroundPoint(Vector2 posit)
        {
            var points = new List<Vector2>();

            for (var i = posit.x - 10;  i < posit.x+10; i++)
                for (var j = posit.y - 10;  j < posit.y + 10; j++)
                {
                    points.Add(new Vector2(i,j));
                }
            return points;
        } 
    }
}
