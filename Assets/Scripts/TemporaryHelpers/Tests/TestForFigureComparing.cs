﻿using System.Collections.Generic;
using Assets.Scripts.GameFigure.FigureComparing;
using UnityEngine;

namespace Assets.Scripts.TemporaryHelpers.Tests
{
    public class TestForFigureComparing : MonoBehaviour
    {
        private ComparingCenter _comparingCenter;
        private List<Vector2> _figure1;
        private List<Vector2> _figure2;

        void Start()
        {
            _comparingCenter = new ComparingCenter(0.22f);
            _figure1 = new List<Vector2>
            {
                new Vector2(0, 0),
                new Vector2(0, 101),
                new Vector2(100, 99),
                new Vector2(100, 0)
            };

            _figure2 = new List<Vector2>
            {
                new Vector2(0, 100),
                new Vector2(50, 50),
                new Vector2(50, 0),
                new Vector2(0, 0)
            };
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                print("Test compare : " +_comparingCenter.CheckFiguresOnSimilarity(_figure1, _figure2));
            }
        }
    }
}
