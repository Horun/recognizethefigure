﻿using Assets.Scripts.FigureEditor;
using UnityEngine;

namespace Assets.Scripts.TemporaryHelpers.Tests
{
    public class TestForFigureSaving : MonoBehaviour
    {
        private EditorCenter _figureEditor;

        void Start()
        {
            _figureEditor = FindObjectOfType<EditorCenter>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                _figureEditor.SaveFigure("SomeFigure");
            }
        }
    }
}
