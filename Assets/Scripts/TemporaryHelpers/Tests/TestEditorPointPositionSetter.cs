﻿using System.Collections.Generic;
using Assets.Scripts.FigureEditor;
using UnityEngine;

namespace Assets.Scripts.TemporaryHelpers.Tests
{
    public class TestEditorPointPositionSetter : MonoBehaviour
    {
        public string Button = "Q";
        private SetterPositionForEditorPoints _positionSetter;
        private List<Vector2> _figureVertices; 

        void Start()
        {
            _positionSetter = FindObjectOfType<SetterPositionForEditorPoints>();
            _figureVertices = new List<Vector2>
            {
                new Vector2(0, 0),
                new Vector2(0, 100),
                new Vector2(100, 100),
                new Vector2(100, 0)
            };
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                _positionSetter.SetPoints(_figureVertices);
            }
        }
    }
}
