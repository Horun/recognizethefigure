﻿using Assets.Scripts.FigureEditor;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.TemporaryHelpers.Tests
{
    public class TestEditorPoints : MonoBehaviour
    {
        private EditorPoint[] _editorPoints;
        private TextureManager _editorTexture;

        void Start()
        {
            _editorPoints = FindObjectsOfType<EditorPoint>();
            _editorTexture = FindObjectOfType<TextureManager>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                foreach (var point in _editorPoints)
                {
                    print(point.GetUnderlyingTexturePixelPosition());
                }
            }
        }
    }
}
