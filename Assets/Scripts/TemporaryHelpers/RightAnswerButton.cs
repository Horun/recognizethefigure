﻿using Assets.Scripts.GameManager;
using UnityEngine;

namespace Assets.Scripts.TemporaryHelpers
{
    public class RightAnswerButton : MonoBehaviour
    {
        private GameCenter _gameCenter;

        void Start()
        {
            _gameCenter = FindObjectOfType<GameCenter>();
        }

        public void OnClick()
        {
            _gameCenter.SwitchFigure();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                OnClick();
            }
        }
    }
}