﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Boomlagoon.JSON;

namespace Assets.Scripts.FigureEditor
{
    public class FigureSaver
    {
        private readonly string _savingPath;
        public static string LastSavedFigureName { get; private set; }

        public FigureSaver()
        {
            _savingPath = PathwaysCenter.GetPathwayToSavedFigures();
        }

        public FigureSaver(string path)
        {
            _savingPath = path;
        }


        public void SaveFigure(string figureName ,List<Vector2> vertices)
        {
            var verticesJSON = new JSONArray();
            foreach (var vertix in vertices)
            {
                verticesJSON.Add(ConvertToJSON(vertix));
            }
            Save(figureName,verticesJSON);
        }

        private JSONArray ConvertToJSON(Vector2 vertix)
        {
            return new JSONArray {(int) vertix.x, (int) vertix.y};
        }

        private void Save(string figureName,JSONArray figure)
        {
            File.WriteAllText(_savingPath+ figureName +".json", figure.ToString());
            LastSavedFigureName = figureName;
        }

        public List<Vector2> GetLastSavedFigureVertices()
        {
            return GetSavedFigureVertices(LastSavedFigureName);
        } 

        public List<Vector2> GetSavedFigureVertices(string figureName)
        {
            var loadingPath = _savingPath + figureName + ".json";
            if (!File.Exists(loadingPath))
                return null;

            var figureJson = JSONArray.Parse(File.ReadAllText(loadingPath));
            return ConvertToFigure(figureJson);
        }

        private List<Vector2> ConvertToFigure(JSONArray figureJson)
        {
            var figureVertices = new List<Vector2>();
            foreach (var vertixInJson in figureJson)
            {
                var asJsonArray = JSONArray.Parse(vertixInJson.ToString());
                var vertix = new Vector2((float) asJsonArray[0].Number, (float) asJsonArray[1].Number);
                figureVertices.Add(vertix);
            }
            return figureVertices;
        } 
    }
}
