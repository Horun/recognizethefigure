﻿using UnityEngine;

namespace Assets.Scripts.FigureEditor
{
    public class PathwaysCenter
    {
        public static string GetPathwayToSavedFigures()
        {
#if UNITY_EDITOR
            return PathToResources;
#endif
            return PathToLocalFolder;
        }


        private const string PathToResources = @"H:\UnityProjects\RecognizeTheFigure\Assets\Resources\SavedFigures\";

        private static string PathToLocalFolder {get { return Application.dataPath; } } 
    }
}
