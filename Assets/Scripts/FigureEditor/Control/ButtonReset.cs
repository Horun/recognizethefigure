﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.FigureEditor.Control
{
    public class ButtonReset : MonoBehaviour {

        public void OnClick()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
