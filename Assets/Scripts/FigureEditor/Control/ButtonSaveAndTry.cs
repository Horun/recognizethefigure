﻿using Assets.Scripts.GameManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.FigureEditor.Control
{
    public class ButtonSaveAndTry : MonoBehaviour
    {
        public InputField FigureName;
        public string NextScene = "MainScene";

        private EditorCenter _editorCenter;
   
        void Start()
        {
            _editorCenter = FindObjectOfType<EditorCenter>();
        }

        public void OnClick()
        {
            var savingFigureVertices =  _editorCenter.SaveFigure(FigureName.text);
            GameCenter.GameMode = GameMode.EditorMode;

            if (savingFigureVertices.Count >= 3) // at least must be triangle
                SceneManager.LoadScene(NextScene);
        }
    }
}
