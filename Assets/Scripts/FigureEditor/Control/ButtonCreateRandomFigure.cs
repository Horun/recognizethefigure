﻿using Assets.Scripts.GameFigure.ConcretFigure;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.FigureEditor.Control
{
    public class ButtonCreateRandomFigure : MonoBehaviour
    {
        private TextureManager _textureManager;
        private EditorCenter _editorCenter;
        private SetterPositionForEditorPoints _editPointsSetter;

        void Start()
        {
            _textureManager = FindObjectOfType<TextureManager>();
            _editorCenter = FindObjectOfType<EditorCenter>();
            _editPointsSetter = FindObjectOfType<SetterPositionForEditorPoints>();
        }

        public void OnClick()
        {
            var middlePointForDrawing = new Vector2(_textureManager.TextureWidth/2f, _textureManager.TextureHeight/2f);
            var radius = _textureManager.TextureWidth/3;

            var randFigure = GetterRandomVertices.GetRandomFigureVertices(radius, middlePointForDrawing);

            _editPointsSetter.RemoveAllPoints();
            _editPointsSetter.SetPoints(randFigure);
            _editorCenter.ShowFigure();
        }

    }
}

