﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.FigureEditor.Control
{
    public class ButtonLoadFigure : MonoBehaviour
    {
        public InputField FigureNameForLoading;
        public InputField FigureNameForSaving;

        private FigureSaver _figureSaver;
        private SetterPositionForEditorPoints _positionSetter;
        private EditorCenter _figureEditor;

        void Start()
        {
            _figureSaver = new FigureSaver();
            _positionSetter = FindObjectOfType<SetterPositionForEditorPoints>();
            _figureEditor = FindObjectOfType<EditorCenter>();
        }

        public void OnClick()
        {
            var figureVertices = _figureSaver.GetSavedFigureVertices(FigureNameForLoading.text);
            if(figureVertices==null)
                return;

            _positionSetter.SetPoints(figureVertices);
            _figureEditor.ShowFigure();
            SetSameNameForSaving();
        }

        private void SetSameNameForSaving()
        {
            FigureNameForSaving.text = FigureNameForLoading.text;
        }

    }
}
