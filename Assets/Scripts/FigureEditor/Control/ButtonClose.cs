﻿using Assets.Scripts.GameManager;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.FigureEditor.Control
{
    public class ButtonClose : MonoBehaviour
    {
        public string NextSceneName = "MainScene";

        public void OnClick()
        {
            SceneManager.LoadScene(NextSceneName);
            GameCenter.GameMode = GameMode.GameWithTime;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnClick();
            }
        }
    }
}
