﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.FigureEditor.Control
{
    public class ButtonSave : MonoBehaviour
    {   
        public InputField NameField;

        private EditorCenter _figureEditor;

        void Start()
        {
            _figureEditor = FindObjectOfType<EditorCenter>();
            NameField.text = "MyFigure";
        }

        public void OnClick()
        {
            _figureEditor.SaveFigure(NameField.text);
        }
    }
}
