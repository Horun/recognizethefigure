﻿using UnityEngine;

namespace Assets.Scripts.FigureEditor.Control
{
    public class MouseControl : MonoBehaviour
    {
        private Camera _cam;
        private readonly string _editorPointTag = "EditorPoint";
        private Transform _catchedPoint;
        private int _layerMask;

        void Start()
        {
            _cam = Camera.main;//кеширование
            _layerMask = 1 << LayerMask.NameToLayer("Default");
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var touchedObject = Physics2D.Raycast(_cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero,50,_layerMask);

                if ((touchedObject.transform != null) && touchedObject.transform.tag == _editorPointTag)
                    _catchedPoint = touchedObject.transform;
            }

            if (Input.GetMouseButtonUp(0))
                _catchedPoint = null;

            if (_catchedPoint != null)
            {
                var mouseWorldPosit = _cam.ScreenToWorldPoint(Input.mousePosition);
                _catchedPoint.position = new Vector2(mouseWorldPosit.x, mouseWorldPosit.y); //z must be 0
            }
        }


    }
}


