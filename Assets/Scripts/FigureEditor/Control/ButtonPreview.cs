﻿using UnityEngine;

namespace Assets.Scripts.FigureEditor.Control
{
    public class ButtonPreview : MonoBehaviour
    {
        private EditorCenter _figureEditor;

        void Start()
        {
            _figureEditor = FindObjectOfType<EditorCenter>();
        }

        public void OnClick()
        {
            _figureEditor.ShowFigure();
        }
    }
}
