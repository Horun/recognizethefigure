﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameFigure;
using Assets.Scripts.GameManager;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.FigureEditor
{
    public class EditorCenter : MonoBehaviour
    {
        public TextureManager EditorTexture;

        private EditorPoint[] _editorPoints;
        private Painter _painter;
        private FigureSaver _figureSaver;

        void Start()
        {
            _editorPoints = FindObjectsOfType<EditorPoint>().OrderBy( point => point.Number).ToArray();
            _painter = new Painter();
            _figureSaver = new FigureSaver();
            FindObjectOfType<TexturePreparer>().Prepare();

            CheckMaybeNeedLoadLastFigure();
        }

        private void CheckMaybeNeedLoadLastFigure()
        {
            //Editor mode set when try new figure from editor still player leaves Editor by close button
            if (GameCenter.GameMode != GameMode.EditorMode)
                return;

            var lastFigureVertices = _figureSaver.GetLastSavedFigureVertices();
            DrawFigure(lastFigureVertices);
            FindObjectOfType<SetterPositionForEditorPoints>().SetPoints(lastFigureVertices);
            EditorTexture.Clean();
            DrawFigure(lastFigureVertices);
        }

        public void ShowFigure()
        {
            EditorTexture.Clean();
            var figureVertices = GetFigureVerticesInRightOrder();
            DrawFigure(figureVertices);
        }

        private List<Vector2> GetFigureVerticesInRightOrder()
        {
            var pixelsPosit = new List<Vector2>();

            foreach (var editPoint in _editorPoints)
            {
                var underlyingPixel = editPoint.GetUnderlyingTexturePixelPosition();
                if(underlyingPixel != editPoint.NoAnyPixelsValue)
                    pixelsPosit.Add(underlyingPixel);
            }
            return pixelsPosit;
        }

        private void DrawFigure(List<Vector2> figureVertices)
        {
            for (int i = 0; i < figureVertices.Count; i++)
            {
                var nextVertix = i == figureVertices.Count - 1 ? 0 : i+1;
                var figureSide = GetterLinePoints.GetPixelsPositonOnTextureBetween(figureVertices[i],
                    figureVertices[nextVertix], EditorTexture);
                _painter.Draw(figureSide , EditorTexture);
            }
        }

        public List<Vector2> SaveFigure(string figureName)
        {
            var vertices = GetFigureVerticesInRightOrder();
            _figureSaver.SaveFigure(figureName, vertices);
            return vertices;
        }
    }
}
