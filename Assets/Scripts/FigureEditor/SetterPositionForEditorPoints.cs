﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.FigureEditor
{
    public class SetterPositionForEditorPoints : MonoBehaviour
    {
        private EditorPoint[] _editorPoints;
        private Dictionary<EditorPoint, Vector3> _pointsStartPositions; 

        void Start()
        {
            _editorPoints = FindObjectsOfType<EditorPoint>().OrderBy(p => p.Number).ToArray();
            _pointsStartPositions = new Dictionary<EditorPoint, Vector3>();
            foreach (var editPoint in _editorPoints)
            {
                _pointsStartPositions.Add(editPoint, editPoint.transform.position);
            }
        }

        public void SetPoints(List<Vector2> figureVertices)
        {
            if(figureVertices.Count>_editorPoints.Length)
                return;

            for (var i = 0; i < figureVertices.Count; i++)
            {
                _editorPoints[i].transform.position = figureVertices[i]/100f;//100 pixel = 1 world point(meter)
            }
        }

        public void RemoveAllPoints()
        {
            foreach (KeyValuePair<EditorPoint, Vector3> editPointAndPosit in _pointsStartPositions)
            {
                editPointAndPosit.Key.transform.position = editPointAndPosit.Value;
            }
        }
    }
}
