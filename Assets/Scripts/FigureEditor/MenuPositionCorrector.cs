﻿using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.FigureEditor
{

    public class MenuPositionCorrector : MonoBehaviour
    {
        public Transform TargetMenu;

        void Start()
        {
            var leftTexturePosit = GetLeftMostPositionOfTexture();
            var leftScreenBorderPosit = GetLeftScreenBorderPosition();
            var posX = (leftTexturePosit + leftScreenBorderPosit)/2f;
            TargetMenu.position = new Vector3(posX, TargetMenu.position.y, TargetMenu.position.z);
        }

        private float GetLeftMostPositionOfTexture()
        {
            var editorTexture = FindObjectOfType<TextureManager>();
            var width = editorTexture.TextureWidth/100f; // 1 world point = 100 texture pixels
            return editorTexture.transform.position.x - width/2f;
        }

        private float GetLeftScreenBorderPosition()
        {
            return Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x;
        }
       
    }
}
