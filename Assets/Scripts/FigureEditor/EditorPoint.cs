﻿using UnityEngine;

namespace Assets.Scripts.FigureEditor
{
    public class EditorPoint : MonoBehaviour
    {
        public int Number;
        public Vector2 NoAnyPixelsValue { get; private set; }
        private int _layerMask;

        void Start()
        {
            _layerMask = LayerMask.GetMask("UI");
            NoAnyPixelsValue = new Vector2(-1, -1);
        }

        public Vector2 GetUnderlyingTexturePixelPosition()
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.zero, 20, _layerMask); ;

            var boxCollider = hit.collider as BoxCollider2D;

            if (boxCollider == null)
                return NoAnyPixelsValue;

            return hit.point * 100; // 1 world point = 100 texture pixels        
        }
    }
}
