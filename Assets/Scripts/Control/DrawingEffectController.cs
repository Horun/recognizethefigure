﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Control
{
    public class DrawingEffectController : MonoBehaviour
    {
        public GameObject CircleModel;
        public int CirclesCount;

        private Transform[] _circles;
        private List<Vector2> _circlePositions;  
        public bool Started { get; private set; }
        private Camera _cam;

        void Start()
        {
            _circles = MultiplyCircleModel();
            PrepareCircles(_circles);
            _circlePositions = new List<Vector2>();
            _cam = Camera.main;
        }

        private Transform[] MultiplyCircleModel()
        {
            var multipliedCircles = new Transform[CirclesCount];
            for (int i = 0; i < CirclesCount; i++)
            {
                multipliedCircles[i] = Instantiate(CircleModel).transform;
            }

            return multipliedCircles;
        }

        private void PrepareCircles(Transform[] circles)
        {
            for (int i = 0; i < circles.Length; i++)
            {
                circles[i].localScale = Vector3.one * (1 - i / (circles.Length + 1f));
            }
        }

        public void StartEffect()
        {
            Started = true;
            _circlePositions.Clear();
        }

        public void StopEffect()
        {
            Started = false;
            HideCircles();
        }

        void Update()
        {
            if (!Started)
                return;

            if (_circlePositions.Count == _circles.Length)
                _circlePositions.RemoveAt(0);

            var nextPosit = _cam.ScreenToWorldPoint(Input.mousePosition);
            _circlePositions.Add(nextPosit);

            for (int i = 0; i < _circlePositions.Count; i++)
            {
                _circles[i].position = _circlePositions[_circlePositions.Count - 1 - i];
            }
        }

        private void HideCircles()
        {
            var hidePoisition = new Vector2(-100, -100);
            for (int i = 0; i < _circles.Length; i++)
            {
                _circles[i].position = hidePoisition;
            }
        }

    }
}
