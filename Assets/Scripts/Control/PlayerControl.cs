﻿using System.Collections.Generic;
using Assets.Scripts.GameFigure.FigureComparing;
using Assets.Scripts.GameManager;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.Control
{
    public class PlayerControl : MonoBehaviour
    {
        public TextureManager TextureForPainting;
        protected Vector2 PreviousSelectedPixel;
        private Painter _painter;
        private readonly List<Vector2> _drawedPoints = new List<Vector2>();
        private GameCenter _gameCenter;

        protected virtual void Start()
        {   
            _painter = new Painter();
            _gameCenter = FindObjectOfType<GameCenter>();
        }

        public void CheckSelectedPixel(Vector2 pixelPosit)
        {
            _drawedPoints.Add(pixelPosit);
            var lastDrawedLine = GetterLinePoints.GetPixelsPositonOnTextureBetween(PreviousSelectedPixel, pixelPosit,
                TextureForPainting);
            //_drawedPoints.AddRange(lastDrawedLine);

            _painter.Draw(lastDrawedLine, TextureForPainting);
        }

        protected void ResetFindings()
        {
            _drawedPoints.Clear();            
        }

        public List<Vector2> GetPaintedPoints()
        {
            return _drawedPoints;
        }

        protected void CompareMyFigureAccordantToNeed()
        {
            if (_drawedPoints.Count > 2)
                _gameCenter.CheckFiguresForSimilarities(_drawedPoints);
        }
    }
}
