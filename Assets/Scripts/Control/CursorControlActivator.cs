﻿using UnityEngine;

namespace Assets.Scripts.Control
{
    public class CursorControlActivator : MonoBehaviour {

        //class need for runtime adding cursor control with settings from editor
        // CursorControl cant be enbled on object, only add or remove

        public Texture2D TextureOnEnter;
        public Texture2D TextureOnMouseDown;
        public Texture2D TextureOnMouseUp;

        public Vector2 HotSpotOnEnter = Vector2.zero;
        public Vector2 HotSpotOnDown = Vector2.zero;
        public Vector2 HotSpotOnUp = Vector2.zero;

        private CursorControl _cursorControl;

        public void Activate()
        {
            CreateCursorControl();
        }

        private void CreateCursorControl()
        {
            _cursorControl = gameObject.AddComponent<CursorControl>();
            _cursorControl.TextureOnEnter = TextureOnEnter;
            _cursorControl.TextureOnMouseDown = TextureOnMouseDown;
            _cursorControl.TextureOnMouseUp = TextureOnMouseUp;

            _cursorControl.HotSpotOnUp = HotSpotOnUp;
            _cursorControl.HotSpotOnDown = HotSpotOnDown;
            _cursorControl.HotSpotOnEnter = HotSpotOnEnter;
        }

        public void Disactivate()
        {
            if (_cursorControl != null)
                Destroy(_cursorControl);
        }
    }
}
