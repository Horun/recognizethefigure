﻿using UnityEngine;

namespace Assets.Scripts.Control
{
    [RequireComponent(typeof(DrawingEffectController))]
    public class MouseControl : PlayerControl
    {
        private Camera _cam;
        private readonly Vector2 _noAnyTexturePixel = new Vector2(-1, -1);
        private DrawingEffectController _effectController;

        protected override void Start()
        {
            base.Start();
            _cam = Camera.main; //cashing
            _effectController = GetComponent<DrawingEffectController>();
        }

        void Update () {

            if (Input.GetMouseButtonDown(0))
            {
                ResetFindings();
                PreviousSelectedPixel = GetPixelUnderTheMouse();
                _effectController.StartEffect();
            }

            if (Input.GetMouseButton(0))
            {
                //can be issue when player press down before activate
                var currentPixelPosit = GetPixelUnderTheMouse();
                if (currentPixelPosit == _noAnyTexturePixel)
                {
                    _effectController.StopEffect();
                    return;
                }
                else if(! _effectController.Started)
                {
                    _effectController.StartEffect();
                }
                CheckSelectedPixel(currentPixelPosit);
                PreviousSelectedPixel = currentPixelPosit;
            }

            if (Input.GetMouseButtonUp(0))
            {
                CompareMyFigureAccordantToNeed();
                _effectController.StopEffect();
            }
        }

        private Vector2 GetPixelUnderTheMouse()
        {
            RaycastHit2D hit = Physics2D.Raycast(_cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            var boxCollider = hit.collider as BoxCollider2D;

            if (boxCollider == null)
                return _noAnyTexturePixel;

            return hit.point*100; // 1 world point = 100 texture pixels        
        }

    }
}
