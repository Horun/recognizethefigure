﻿using UnityEngine;

namespace Assets.Scripts.Control
{
    [RequireComponent(typeof(Collider2D))]
    public class CursorControl : MonoBehaviour
    {
        public Texture2D TextureOnEnter;
        public Texture2D TextureOnMouseDown;
        public Texture2D TextureOnMouseUp;

        public Vector2 HotSpotOnEnter = Vector2.zero;
        public Vector2 HotSpotOnDown = Vector2.zero;
        public Vector2 HotSpotOnUp = Vector2.zero;

        public CursorMode CursorMode = CursorMode.Auto;

        void Start()
        {
            if(IsMouseUnderObject())
                Cursor.SetCursor(TextureOnEnter, HotSpotOnEnter, CursorMode);
        }

        private bool IsMouseUnderObject()
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            return (hit.transform == transform);
        }

        void OnMouseEnter()
        {
            Cursor.SetCursor(TextureOnEnter, new Vector2(0,-TextureOnEnter.height), CursorMode);
        }

        void OnMouseExit()
        {
            Cursor.SetCursor(null, HotSpotOnEnter, CursorMode);
        }

        void OnMouseDown()
        {
            Cursor.SetCursor(TextureOnMouseDown, HotSpotOnDown, CursorMode);
        }

        void OnMouseUp()
        {
            Cursor.SetCursor(TextureOnMouseDown, HotSpotOnUp, CursorMode);
        }

        void OnDestroy()
        {
            Cursor.SetCursor(null, HotSpotOnEnter, CursorMode);
        }
    }
}
