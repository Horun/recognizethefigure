﻿using Assets.Scripts.GameManager;
using UnityEngine;

namespace Assets.Scripts.Buttons
{
    public class ButtonRestart : MonoBehaviour
    {
        public GameObject RestartMenu;
        private GameCenter _gameCenter;
        void Start()
        {
            _gameCenter = FindObjectOfType<GameCenter>();
        }

        public void OnClick()
        {
            _gameCenter.StartGame();
            RestartMenu.SetActive(false);
        }
    }
}
