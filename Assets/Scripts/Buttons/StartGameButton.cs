﻿using Assets.Scripts.GameManager;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.Buttons
{
    public class StartGameButton : MonoBehaviour
    {
        public GameObject[] MustBeActivated;
        public GameObject[] MustBeDisactivated;

        private GameCenter _gameCenter;

        void Start()
        {
            _gameCenter = FindObjectOfType<GameCenter>();
            FindObjectOfType<TexturePreparer>().Prepare();
        }

        public void StartGame()
        {
            _gameCenter.Init();
            _gameCenter.StartGame();
            SetActive(MustBeActivated, true);
            SetActive(MustBeDisactivated, false);

            gameObject.SetActive(false);
        }

        private void SetActive(GameObject[] targets, bool active)
        {
            foreach (var targ in targets)
            {
                if(targ.activeSelf != active)
                    targ.SetActive(active);
            }
        }
    }
}
