﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Buttons
{
    public class GoToEditorButton : MonoBehaviour
    {
        public string EditorScene = "EditorScene";

        public void OnClick()
        {
            SceneManager.LoadScene(EditorScene);
        }
    }
}
