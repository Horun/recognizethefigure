﻿using Assets.Scripts.GameManager;
using Assets.Scripts.GameTexture;
using UnityEngine;

namespace Assets.Scripts.Buttons
{
    public class UiPreparer : MonoBehaviour
    {
        //diff buttons must be active at different play mode(game with time 
        // or testing figure from editor)
        public GameObject[] ModeGameWithTimeUi;
        public GameObject[] ModeEditorUi;

        void Start()
        {
            SetActive(ModeGameWithTimeUi, GameCenter.GameMode == GameMode.GameWithTime);
            SetActive(ModeEditorUi, GameCenter.GameMode == GameMode.EditorMode);

            if (GameCenter.GameMode == GameMode.EditorMode)
            {
                FindObjectOfType<TexturePreparer>().Prepare();
                var gameCenter = FindObjectOfType<GameCenter>();
                gameCenter.Init();
                gameCenter.StartGame();
            }
        }

        private void SetActive(GameObject[] targets ,bool active)
        {
            foreach (var target in targets)
            {
                if(target.activeSelf != active)
                    target.SetActive(active);
            }
        }
    }
}
