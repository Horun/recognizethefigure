﻿using UnityEngine;

namespace Assets.Scripts.Time
{
    public class TimeManager : MonoBehaviour
    {
        public float Time { get; private set; }
        private float _decrAmount; //decreasing per figure time amout

        public void Init (float startTime, float decreasingAmount)
        {
            Time = startTime;
            _decrAmount = decreasingAmount;
        }

        public void PrepareTimeForNextFigure()
        {
            Time = (Time - _decrAmount) > 0 ? Time - _decrAmount : 0;
        }
    }
}
