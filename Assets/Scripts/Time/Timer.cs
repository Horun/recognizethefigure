﻿using System;
using Assets.Scripts.GameManager;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Time
{
    public class Timer : MonoBehaviour
    {
        public TextMesh TimeText; 

        private float _time;
        private bool _isRun;
        private GameCenter _gameCenter;

        public void Init(GameCenter gameCenter)
        {
            _gameCenter = gameCenter;
        }

        public void Run(float startTime) 
        {
            _time = startTime;
            _isRun = true;
            TimeText.text = ((int) startTime).ToString();
        }

        void Update()
        {
            if (!_isRun)
                return;

            DecreaseTime();
            RefreshTimeOnUi(_time);
        }

        private void DecreaseTime()
        {
            _time = _time - UnityEngine.Time.deltaTime > 0
                ? _time - UnityEngine.Time.deltaTime 
                : 0;
        }

        private void RefreshTimeOnUi(float newValue)
        {
            if (Math.Abs(newValue) < 0.0001)
            {
                StopTimer();
                return;
            }
            //show seconds whole number (целое число секунд)
            var timeOnUi =(int) float.Parse(TimeText.text);
            var needValue = (int) (newValue+1);

            if (needValue != timeOnUi)
                TimeText.text = needValue.ToString();
        }

        private void StopTimer()
        {
            TimeText.text = 0.ToString();
            _isRun = false;
        }

    }
}
