﻿namespace Assets.Scripts.GameManager
{
    public class PlayerScore 
    {
        public int Score { get; private set; }

        public void AddScore(int count)
        {
            Score += count;
        }

        public void Reset()
        {
            Score = 0;
        }
    }
}
