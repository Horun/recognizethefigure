﻿using Assets.Scripts.AudioControl;
using Assets.Scripts.Control;
using UnityEngine;

namespace Assets.Scripts.GameManager
{
    public class LosingCenter : MonoBehaviour
    {
        public GameObject RestartMenu;
        private PlayerScore _playerScore;
        private ScoreShower _scoreShower;
        private PlayerControl _playerControl;
        private AudioManager _audioManager;
        private float _time;
        private bool _timerRun;

        public void Init(PlayerScore playerScore, PlayerControl playerControl, AudioManager audioManager)
        {
            _playerScore = playerScore;
            _scoreShower = FindObjectOfType<ScoreShower>();
            _playerControl = playerControl;
            _audioManager = audioManager;
        }

        public void StartTimerToLosing(float timeToLosing)
        {
            _timerRun = true;
            _time = timeToLosing;
        }


        void Update()
        {
            if(!_timerRun)
                return;
            _time -= UnityEngine.Time.deltaTime;
            
            if(_time <= 0)
                PlayerLose();
        }

        private void PlayerLose()
        {
            RestartMenu.SetActive(true);
            ShowPlayerResult();
            _playerControl.enabled = false;
            _playerControl.GetComponent<DrawingEffectController>().StopEffect();
            FindObjectOfType<CursorControlActivator>().Disactivate();
            _audioManager.StopBackGroundSound();
            //_audioManager.PlayTimeOver();
        }

        private void ShowPlayerResult()
        {
            _scoreShower.ShowScore(_playerScore.Score);
        }

        public void StopLosingTime()
        {
            _timerRun = false;
        }
    }
}
