﻿using UnityEngine;

namespace Assets.Scripts.AudioControl
{
    public class AudioManager : MonoBehaviour
    {
        public AudioClip ButtonClick;
        public AudioClip CorrectAnswer;
        public AudioClip WrongAnswer;
        public AudioClip TimeOver;
        public AudioClip BackGroundSound;

        private AudioSource _mainSource;
        private AudioSource _backGroundSource;

        void Awake()
        {
            _mainSource = gameObject.AddComponent<AudioSource>();
            _mainSource.playOnAwake = false;
            _backGroundSource = gameObject.AddComponent<AudioSource>();
            _backGroundSource.playOnAwake = false;
            _backGroundSource.loop = true;
            _backGroundSource.clip = BackGroundSound;
        }

        public void PlayButtonClick()
        {
            Play(ButtonClick);
        }

        public void PlayCorrectAnswer()
        {
            Play(CorrectAnswer);
        }

        public void PlayWrongAnswer()
        {
            Play(WrongAnswer);
        }

        public void PlayTimeOver()
        {
            Play(TimeOver);
        }

        private void Play(AudioClip clip)
        {
            _mainSource.clip = clip;
            _mainSource.Play();
        }

        public void PlayBackGroundSound()
        {
            _backGroundSource.Play();
        }

        public void StopBackGroundSound()
        {
            _backGroundSource.Stop();
        }

    }
}
