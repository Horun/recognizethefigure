﻿using System.Collections.Generic;
using Assets.Scripts.AudioControl;
using Assets.Scripts.Control;
using Assets.Scripts.GameFigure;
using Assets.Scripts.GameFigure.FigureComparing;
using Assets.Scripts.GameTexture;
using Assets.Scripts.Time;
using UnityEngine;

namespace Assets.Scripts.GameManager
{
    public enum GameMode
    {
        GameWithTime,
        EditorMode //Load figure from editor
    }

    public class GameCenter : MonoBehaviour
    {
        public static GameMode GameMode;
        public TextureManager PaintingTexture;
        public PlayerControl PlayerControl;
        public float PossibleDeviation = 0.19f;

        private FigureShower _figureShower;
        private FigureGetter _figureGetter;
        private TimeManager _timeManager;
        private Timer _timer;
        private ComparingCenter _figureComparer;
        private Figure _neededFigure;
        private PlayerScore _playerScore;
        private LosingCenter _losingCenter;
        private AudioManager _audioManager;

        private float _startTimeShowing = 15f;
        private float _decreasingValue = 1f;

        public void Init()
        {
            _figureGetter = new FigureGetter(PaintingTexture.TextureWidth/1.5f);
            _figureShower = FindObjectOfType<FigureShower>();
            _figureShower.Init();
            _timeManager = FindObjectOfType<TimeManager>();

            _timer = FindObjectOfType<Timer>();
            _figureComparer = new ComparingCenter(PossibleDeviation);
            _playerScore = new PlayerScore();
            _audioManager = GetComponent<AudioManager>();

            _losingCenter = FindObjectOfType<LosingCenter>();
            _losingCenter.Init(_playerScore, PlayerControl,_audioManager);      
        }

        public void StartGame()
        {
            ActivatePlayer();
            PrepareCursor();
            _playerScore.Reset();
            _audioManager.PlayBackGroundSound();
            _timeManager.Init(_startTimeShowing, _decreasingValue);
            PaintingTexture.Clean();
            _figureGetter.Reset();
            SwitchFigure();
        }

        private void PrepareCursor()
        {
            var cursorActivator = PaintingTexture.GetComponent<CursorControlActivator>();
            cursorActivator.Activate();
        }

        private void ActivatePlayer()
        {
            PlayerControl.enabled = true;
        }

        public void SwitchFigure()
        {
            var nextFigure = _figureGetter.GetFigure(GameMode);
            _figureShower.ShowFigure(nextFigure);

            _timer.Run(_timeManager.Time);
            _losingCenter.StartTimerToLosing(_timeManager.Time);
            _timeManager.PrepareTimeForNextFigure();
            _neededFigure = nextFigure;
        }

        public void CheckFiguresForSimilarities(List<Vector2> playerFigure)
        {
            if (_figureComparer.CheckFiguresOnSimilarity(_neededFigure.FigurePoints, playerFigure))
            {
                _losingCenter.StopLosingTime();
                _playerScore.AddScore(1);
                SwitchFigure();
                _audioManager.PlayCorrectAnswer();    
            }
            else
            {
                _audioManager.PlayWrongAnswer();           
            }

            PaintingTexture.Clean();
        }

    }
}
