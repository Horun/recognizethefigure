﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GameManager
{
    public class ScoreShower : MonoBehaviour
    {
        public Text ScoreText;

        public void ShowScore(int score)
        {
            ScoreText.text ="Score : " + score.ToString();
        }        
    }
}
