﻿using UnityEngine;

namespace Assets.Scripts.GameTexture
{
    public class TexturePreparer : MonoBehaviour
    {
        public bool OnAwake;

        void Awake()
        {
            if(OnAwake)
                Prepare();
        }

        public void Prepare()
        {
            var textures = GetTexture();
            foreach (var texture in textures)
            {
                texture.Init();
            }
        }

        private TextureManager[] GetTexture()
        {
            return FindObjectsOfType<TextureManager>();
        }

    }
}
