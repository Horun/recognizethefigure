﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameTexture
{
    public class TextureManager : MonoBehaviour
    {
        public Texture2D DefaultTexture;//have no changes, simply keeps default state 
        
        public Texture2D Texture;
        public int TextureWidth { get { return Texture.width; } }
        public int TextureHeight { get { return Texture.height; } }

        private List<Vector2> _changedPixels = new List<Vector2>();

        public void Init()
        {
            for (var y = 0; y < TextureHeight; y++)
                for (var x = 0; x < TextureWidth; x++)
                {
                    var defaultPixel = DefaultTexture.GetPixel(x, y);
                    Texture.SetPixel(x,y,defaultPixel);
                }
            Texture.Apply();
        }

        public void Clean()
        {
            for (var i = 0; i < _changedPixels.Count; i++)
            {
                var changedPosit = _changedPixels[i];
                var samePixelDefualt = DefaultTexture.GetPixel((int)changedPosit.x, (int)changedPosit.y);
                Texture.SetPixel((int)changedPosit.x, (int)changedPosit.y, samePixelDefualt);
            }

            Texture.Apply();
            _changedPixels.Clear();
        }

        public void ChangePixels(List<Vector2> pixelsPosition, Color32 needColor)
        {
            for (var i = 0; i < pixelsPosition.Count; i++)
            {
                var posit = pixelsPosition[i];
                Texture.SetPixel((int)posit.x, (int)posit.y, needColor);
                _changedPixels.Add(posit);
            }
            Texture.Apply();
        }
    }
}
