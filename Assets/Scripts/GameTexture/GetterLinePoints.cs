﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameTexture
{
    public class GetterLinePoints  {

        public static List<Vector2> GetPixelsPositonOnTextureBetween(Vector2 point1, Vector2 point2, TextureManager texture) //texture need for pixel number controlling, some time method calculate wrong values
        {
            var pixelInLine = new List<Vector2>();

            var startX = (int)point1.x;
            var endX = point2.x;
            for (var x = startX; ((startX < endX) && (x <= endX)) || (startX > endX) && (x >= endX); x = startX < endX ? x + 1 : x - 1)
            {
                var y = CalculateY(point1, point2, x);
                if (y >= 0 && y < texture.TextureHeight)
                    pixelInLine.Add(new Vector2(x, y));
            }

            var yStart = (int)point1.y;
            var yEnd = point2.y;
            for (var y = yStart; ((yStart < yEnd) && (y < yEnd)) || ((yStart > yEnd) && (y > yEnd)); y = yStart < yEnd ? y + 1 : y - 1)
            {
                var x = CalculateX(point1, point2, y);
                if (x >= 0 && x < texture.TextureWidth)
                    pixelInLine.Add(new Vector2(x, y));
            }
            return pixelInLine;
        }

        private static int CalculateX(Vector2 point1, Vector2 point2, int y)
        {
           return (int)(((point2.x - point1.x) * y + (point1.x * point2.y - point2.x * point1.y)) / (point2.y - point1.y));
        }

        private static int CalculateY(Vector2 point1, Vector2 point2, int x)
        {
            return (int)(((point1.y - point2.y) * x + (point1.x * point2.y - point2.x * point1.y)) / (point1.x - point2.x));
        }

    }
}
