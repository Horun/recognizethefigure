﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameTexture
{
    public class Painter
    {

        public void Draw(List<Vector2> points, TextureManager textureManager, int brushSize = 2)
        {
            var pointsForDrawing = new List<Vector2>();
            for (var i = 0; i < points.Count; i++)
            {
                pointsForDrawing.AddRange(GetPointsAround((int) points[i].x, (int) points[i].y, brushSize));
            }
            textureManager.ChangePixels(pointsForDrawing, Color.black);
        }

        private List<Vector2> GetPointsAround(int targetX, int targetY, int radius)
        {
            var pointsAround = new List<Vector2>();
            for (int y = targetY - radius; y < targetY + radius; y++)
                for (int x = targetX - radius; x < targetX + radius; x++)
                {
                    pointsAround.Add(new Vector2(x, y));
                }

            return pointsAround;
        }
    }
}
